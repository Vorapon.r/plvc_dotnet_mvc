using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using myproject.Models;

namespace myproject.Controllers;

public class PlvcController : Controller
{
    public IActionResult Index()
    {
        return View(PLVCDatas(""));
    }

    [HttpPost]
    public ActionResult Index(PLVC_Data model)
    {   
        return View(PLVCDatas(model.Name));
    }

    public JsonResult GetData(string name){

        return Json(new {
            Data = PLVCDatas(name)
        });
    }

    public IActionResult Create(){
        return View();
    }

    [HttpPost]
    public IActionResult Create(PLVC_Table model){
        Console.WriteLine("Method: Create || Name: " + model.Name);
        return RedirectToAction("Index");
    }

    public IActionResult Edit(string Id){
        var plvcTable = GetTablePlvc(Id);
        return View(plvcTable);
    }

    [HttpPost]
    public IActionResult Edit(PLVC_Table model){
        Console.WriteLine("Method: Edit || Name: " + model.Name);
        return RedirectToAction("Index");
    }

    public IActionResult Delete(string id){
        Console.WriteLine("Method: Delete || Id: " + id);
        return RedirectToAction("Index");
    }


    [HttpPost]
    public JsonResult Create_Json(PLVC_Table model){
        Console.WriteLine("Method: Create_Json || Name: " + model.Name);
        return Json("Create");
    }

    public IActionResult Edit_Json(string Id){
        var plvcTable = GetTablePlvc(Id);
        return Json(plvcTable);
    }

    [HttpPost]
    public JsonResult Edit_Json(PLVC_Table model){
        Console.WriteLine("Method: Edit_Json || Name: " + model.Name);
        return Json("Edit");
    }


    public PLVC_Data PLVCDatas(string name){
        PLVC_Data data = new PLVC_Data();

        data.Name = "Information";

        data.Tables = new List<PLVC_Table>();
        data.Tables.Add(new PLVC_Table {
            Id = "1",
            Name = "รายการ A",
            Type = "รายการคงเหลือ",
            Amount = 10000,
            Status = "รออนุมัติ"
        });

        data.Tables.Add(new PLVC_Table {
            Id = "2",
            Name = "รายการ B",
            Type = "รายการยอดคงค้าง",
            Amount = 20000,
            Status = "อนุมัติ"
        });

        data.Tables.Add(new PLVC_Table {
            Id = "3",
            Name = "รายการ C",
            Type = "รายการรายจ่าย",
            Amount = 10000,
            Status = "ยกเลิก"
        });

        if(name.ToUpper() == "SEARCH"){
            data.Tables.Add(new PLVC_Table {
                Id = "4",
                Name = "รายการ D",
                Type = "รายการรายจ่าย",
                Amount = 50000,
                Status = "อนุมัติ"
            });
        }

        return data;
    }

    public PLVC_Table GetTablePlvc(string id){
        if(id == "1"){
            return new PLVC_Table
            {
                Id = "1",
                Name = "รายการ A",
                Type = "รายการคงเหลือ",
                Amount = 10000,
                Status = "รออนุมัติ"
            };
        }
        else if(id == "2"){
            return new PLVC_Table
            {
                Id = "2",
                Name = "รายการ B",
                Type = "รายการยอดคงค้าง",
                Amount = 20000,
                Status = "อนุมัติ"
            };
        }
        else{
            return null;
        }
    }

}


public class PLVC_Data{
    public string? Name { get; set; }
    public List<PLVC_Table> Tables { get; set; }
}

public class PLVC_Table{
    public string? Id { get; set; }
    public string? Name { get; set; }
    public string? Type { get; set; }
    public decimal Amount { get; set; }
    public string? Status { get; set; }
}
